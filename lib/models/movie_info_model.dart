class MovieInfoModel {
  int? daysUntil;
  String? overview;
  String? posterUrl;
  String? releaseDate;
  String? title;
  String? type;
  dynamic upcomingInfo;

  MovieInfoModel(
      {this.daysUntil = 0,
      this.overview = "",
      this.posterUrl = "",
      this.releaseDate = "",
      this.title = "",
      this.type = "",
      this.upcomingInfo});

  MovieInfoModel.fromJson(Map<String, dynamic> json) {
    daysUntil = json['days_until'];
    overview = json['overview'];
    posterUrl = json['poster_url'];
    releaseDate = json['release_date'];
    title = json['title'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['days_until'] = daysUntil;
    data['overview'] = overview;
    data['poster_url'] = posterUrl;
    data['release_date'] = releaseDate;
    data['title'] = title;
    data['type'] = type;
    return data;
  }
}
