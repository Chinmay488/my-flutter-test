import "package:flutter/material.dart";
import 'package:intl/intl.dart';
import "package:get/get.dart";
import 'package:practice_flutter/controllers/movie_info_controller.dart';

class DatePickerWidget extends StatelessWidget {
  TextEditingController dateinput = TextEditingController();
  MovieController movieDataController = Get.find();
  // final controller

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
      width: MediaQuery.of(context).size.width * 0.5,
      child: TextField(
        controller: dateinput, //editing controller of this TextField
        decoration: const InputDecoration(
          icon: Icon(Icons.calendar_today), //icon of text field
          labelText: "Enter Date",
        ),
        readOnly: true,
        onTap: () async {
          DateTime? pickedDate = await showDatePicker(
              context: context,
              initialDate: DateTime.now(),
              firstDate: DateTime(
                  2000), //DateTime.now() - not to allow to choose before today.
              lastDate: DateTime(2101));
          // print(pickedDate.toString());
          if (pickedDate != null) {
            String dateString = DateFormat('yyyy-MM-dd').format(pickedDate);
            movieDataController.getMovieInfo(date: dateString);
          }
        },
      ),
    );
  }
}
