import 'package:cached_network_image/cached_network_image.dart';
import "package:flutter/material.dart";
import 'package:get/get.dart';
import '../controllers/movie_info_controller.dart';
import 'date_picker.dart';

class MovieDetails extends StatelessWidget {
  const MovieDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MovieController movieDataController = Get.find();
    return Obx(
      () => Center(
        child: movieDataController.isFetchingForData.value
            ? const CircularProgressIndicator()
            : Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  DatePickerWidget(),
                  Text("title : ${movieDataController.movieInfo.value.title}"),
                  Text(
                      "days until : ${movieDataController.movieInfo.value.daysUntil}"),
                  Text(
                      "release date : ${movieDataController.movieInfo.value.releaseDate}"),
                  Text("type : ${movieDataController.movieInfo.value.type}"),
                  CachedNetworkImage(
                    imageUrl: movieDataController.movieInfo.value.posterUrl!,
                    placeholder: (context, url) =>
                        const CircularProgressIndicator(),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                    height: MediaQuery.of(context).size.height * 0.5,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      var date = movieDataController
                              .movieInfo.value.upcomingInfo.releaseDate ??
                          "";
                      movieDataController.getMovieInfo(date: date);
                    },
                    child: const Text("Next Movie"),
                  )
                ],
              ),
      ),
    );
  }
}
