import "package:flutter/material.dart";
import "package:get/get.dart";
import "../../controllers/movie_info_controller.dart";
import "../../widgets/movie_details.dart";

class Home extends StatelessWidget {
  Home({Key? key}) : super(key: key);
  final movieDataController = Get.put(MovieController());

  @override
  Widget build(BuildContext context) {
    movieDataController.getMovieInfo();
    return const SafeArea(
      child: Scaffold(
        body: MovieDetails(),
      ),
    );
  }
}
