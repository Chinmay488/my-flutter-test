import "package:get/get.dart";
import "../models/movie_info_model.dart";
import "../api/movie_info_api.dart" as api;

class MovieController extends GetxController {
  Rx<MovieInfoModel> movieInfo = MovieInfoModel().obs;
  Rx<bool> isFetchingForData = false.obs;

  Future<dynamic> getMovieInfo({String date = ""}) async {
    isFetchingForData(true);
    dynamic data = await api.getMovieInfo(date: date);
    movieInfo.value = data;
    // isFetchingForData = false as Rx<bool>;/
    movieInfo.refresh();
    isFetchingForData(false);
    return movieInfo.value;
  }
}
