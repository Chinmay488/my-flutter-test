import 'dart:convert';

import "package:http/http.dart" as http;
import 'package:practice_flutter/models/movie_info_model.dart';
import "./links.dart";

Future<dynamic> getMovieInfo({String date = ""}) async {
  String updatedUrl = date == "" ? link : "$link?date=$date";

  Uri url = Uri.parse(updatedUrl);
  http.Response response = await http.get(url);

  if (response.statusCode == 200) {
    var info = jsonDecode(response.body);
    var followingProdiction =
        MovieInfoModel.fromJson(info["following_production"]);
    var data = MovieInfoModel.fromJson(info);
    data.upcomingInfo = followingProdiction;
    return data;
  }
  return null;
}
